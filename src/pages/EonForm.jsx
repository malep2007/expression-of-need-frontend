import React, { useState } from 'react';
import Navbar from '../components/Navbar';

import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useHistory } from 'react-router-dom';

import { getDownloadURL } from 'firebase/storage'

import { uploadFile } from '../firebaseConfig';
import SideBar from '../components/SideBar';
import { Form, Button, Row, Col } from 'react-bootstrap';

function EonForm(props) {
    const [what, setWhat] = useState("")
    const [reasons, setReason] = useState("")
    const [location, setLocation] = useState("")
    const [suppliers, setSuppliers] = useState("")
    const [deliveryDate, setDeliveryDate] = useState("")
    const [quantity, setQuantity] = useState(0)
    const [unitOfMeasurement, setUnitOfMeasurement] = useState("")
    const [budgetEstimate, setBudgetEstimate] = useState()
    const [select, setSelect] = useState(false)
    const [costType, setCostType] = useState()
    const [file, setFile] = useState({})
    const [fileUrl, setFileUrl] = useState("")
    let [progress, setProgress] = useState(0)

    let history = useHistory()

    const isCapex = (capex) => {
        setCostType(capex ? "CAPEX" : "OPEX")
        return costType
    }

    const notify = () => toast('EON Creation Successful')
    const errorNotification = () => toast.error('Please check your inputs')

    const { heading } = { ...props }

    return (
        <>
            <ToastContainer />
            {/* Page Heading */}
            <h1 className="h3 mb-4 text-gray-800">{heading}</h1>
            <Form className="">
                <Row>
                    <Form.Group as={Col}>
                        <Form.Label>Need</Form.Label>
                        <Form.Control
                            onChange={e => {
                                e.preventDefault()
                                setWhat(e.target.value)
                            }}
                            className="form-control" type="text"></Form.Control>
                        <Form.Text className="text-muted">Indicate the purchase request</Form.Text>
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label>Location?</Form.Label>
                        <Form.Control
                            onChange={e => {
                                e.preventDefault()
                                setLocation(e.target.value)
                            }}
                            className="form-control" type="text"></Form.Control>
                        <Form.Text className="text-muted">Indicate where the purchase is required</Form.Text>
                    </Form.Group>
                </Row>
                <Row>
                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
                            onChange={e => {
                                e.preventDefault()
                                setReason(e.target.value)
                            }}
                            as="textarea"></Form.Control>
                        <Form.Text>Give a brief description of the purchase request and reason for the need</Form.Text>
                    </Form.Group>
                </Row>
                <Row>
                    <Form.Group as={Col}>
                    <Form.Label>Recommened Suppliers</Form.Label>
                    <Form.Control 
                        onChange={ e=> {
                            e.preventDefault()
                            setSuppliers(e.target.value)
                        }}
                        className="form-control" type="text"></Form.Control>
                    <Form.Text className="text-muted">You can provide a recommended supplier if any</Form.Text>
                    </Form.Group>
                    <Form.Group as={Col}>
                    <Form.Label>Expected Delivery Date</Form.Label>
                    <Form.Control 
                        onChange={ e=> {
                            e.preventDefault()
                            setDeliveryDate(e.target.value)
                        }}
                        className="form-control" type="date"></Form.Control>
                    <Form.Text className="text-muted">When is expected delivery/ fullfillment of purchase request</Form.Text>
                    </Form.Group>
                </Row>
                <Row>
                    <Form.Group as={Col}>
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control
                            onChange={e => {
                                e.preventDefault()
                                setQuantity(e.target.value)
                            }}
                            className="form-control"
                            type="text"></Form.Control>
                    </Form.Group>
                    <Form.Group as={Col}>
                    <Form.Label>Unit of Measurement</Form.Label>
                        <Form.Control
                            onChange={e => {
                                e.preventDefault()
                                setUnitOfMeasurement(e.target.value)
                            }}
                        ></Form.Control>
                    </Form.Group>
                </Row>
                <div className="divider"></div>
                <Row>
                    <Form.Group>
                        <Form.Label>Budget</Form.Label>
                    </Form.Group>
                </Row>
                <Row>
                    <Form.Group as={Col}>
                        <Form.Label>Estimated Cost</Form.Label>
                        <Form.Control
                            onChange={e => {
                                e.preventDefault()
                                setBudgetEstimate(e.target.value)
                            }}
                        ></Form.Control>
                    </Form.Group>

                    <Form.Group as={Col}>
                        <Form.Label>Budget Type</Form.Label>
                        <Form.Check
                            onChange={e => {
                                e.preventDefault()
                                setSelect(false)
                            }}
                            checked={!select}
                            type="checkbox" label="OPEX">
                        </Form.Check>
                        <Form.Check
                        onChange={e => {
                            e.preventDefault()
                            setSelect(false)
                        }}
                        checked={select}
                        type="checkbox" label="CAPEX"></Form.Check>
                    </Form.Group>
                </Row>
                <Row>
                    <Form.Label>Upload Supporting Documents</Form.Label>
                    <Form.Group md="6" as={Col}>
                        <Form.Control
                            onChange={e => {
                                e.preventDefault()
                                setFile(e.target.files[0])
                            }}
                            type="file"></Form.Control>
                    </Form.Group>

                    <Form.Group as={Col}>
                        <Button
                            onClick={e => {
                                e.preventDefault()
                                const task = uploadFile(file)
                                task.on('state_changed',
                                    (snapshot) => {
                                        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                                        setProgress(progress)
                                        if (progress === 100) {
                                            getDownloadURL(task.snapshot.ref).then((downloadUrl) => {
                                                setFileUrl(downloadUrl)
                                                console.log(`Download URL: ${downloadUrl}`)
                                            })
                                        }
                                        switch (snapshot.state) {
                                            case 'running':
                                                console.log('upload in progress')
                                                break;
                                            case 'success':
                                                console.log('upload completed')
                                                break;
                                        }
                                    },
                                )
                            }}
                        >Upload</Button>
                    </Form.Group>
                    <Form.Group sm="2" as={Col}>
                        <Form.Label>Upload is {progress.toFixed(2)} % complete </Form.Label>
                    </Form.Group>
                </Row>
                <Row>
                    <Form.Group as={Col}>
                        <Button
                            onClick={e => {
                                e.preventDefault()
                                axios.post("http://127.0.0.1:8000/expression/create/",
                                    {
                                        reason: reasons,
                                        location: location,
                                        what: what,
                                        recommended_suppliers: suppliers,
                                        issuer: {
                                            user: {
                                                email: localStorage.getItem("email")
                                            }
                                        },
                                        delivery_date: deliveryDate,
                                        budget_estimate: budgetEstimate,
                                        quantity: quantity,
                                        unit_of_measurement: unitOfMeasurement,
                                        budget_type: isCapex(select),
                                        file_url: fileUrl || 'https://www.google.co.ug'
                                    },
                                    {
                                        headers: {
                                            "Content-type": "Application/json",
                                            "Authorization": `Token ${localStorage.getItem('token')}`
                                        }
                                    }
                                )
                                    .then(response => {
                                        setFileUrl()
                                        notify()
                                        let { from } = { from: { pathname: "/" } }
                                        history.replace(from)
                                    })
                                    .catch(error => {
                                        errorNotification()
                                        console.error(error)
                                    })
                            }}
                        >Submit</Button>
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Button variant="secondary">Cancel</Button>
                    </Form.Group>
                </Row>
                
            </Form>
        </>
    )
}

export default EonForm;