import React from 'react';

import axios from 'axios'
import { useState } from 'react';

function UserSelectDropDown(props) {
    const { id, staff } = { ...props }
    const [email, setEmail] = useState("")

    function assignStaffToExpression() {
        axios.put("http://127.0.0.1:8000/expression/",
            {
                "id": id,
                "staff_assigned": {
                    "user": {
                        "email": email || localStorage.getItem('email')
                    }
                }
            },
            {
                headers: {
                    "Content-type": "Application/json",
                    "Authorization": `Token ${localStorage.getItem('token')}`
                }
            }
        )
            .then(response => console.log("expression updated"))
            .catch(error => console.error("there was an error"))
    }
    return (
        <>
            <form>
                <input
                    onMouseLeave={assignStaffToExpression}
                    className="border rounded-md border-blue-400 p-2 text-xs bg-white hover:bg-blue-500 hover:text-white shadow-md w-20" list="staff"></input>
                <datalist id="staff"
                    name="Procurement Staff">
                    {
                        staff.map(user => (
                            <option
                                key={user.email}
                                onSelect={e => {
                                    e.preventDefault()
                                    console.log('user selected')
                                    setEmail(user.email)
                                }}
                                value={user.username}>{user.first_name}</option>
                        ))
                    }
                </datalist>
            </form>

        </>
    )
}

export default UserSelectDropDown