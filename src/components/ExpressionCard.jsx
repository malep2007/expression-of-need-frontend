import React from 'react';


function ExpressionCard(props) {
    function formatDate(stringDate){
        let date = new Date(stringDate)
        return date.toDateString()
    }

    const { expressions } = { ...props }
    return (
        expressions.map(item => (
            <div key={item.id} className="border md:max-w-md rounded-lg shadow-md flex flex-col">
                <div className="bg-blue-600 w-full h-12 rounded-t-lg text-white font-semibold text-xl p-2 mx-auto text-center capitalize">{item.what}</div>
                <div className="grid grid-cols-2 p-4 text-sm justify-around my-auto">
                    <div className="font-semibold">Reason:</div><div className="font-light capitalize">{item.reason}</div>
                    <div className="font-semibold">Requester:</div><div className="font-light">{item.issuer.user.first_name + " " + item.issuer.user.last_name}</div>
                    <div className="font-semibold">Department:</div><div className="font-light">{item.issuer.department}</div>
                    <div className="font-semibold">Date Submitted:</div><div className="font-light">{formatDate(item.created_at)}</div>
                </div>
                <div className="grid grid-cols-2 p-4 self-center space-x-4 uppercase font-sans font-semibold bg-green-400 w-full text-gray-200 rounded-b-md">
                    <div>status</div><div>{item.status}</div>
                </div>
            </div>
        ))
    )
}

export default ExpressionCard;