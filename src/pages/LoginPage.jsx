import axios from 'axios';
import { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { Col, Row, Card, Form } from 'react-bootstrap';
import Button from '@restart/ui/esm/Button';


function LoginPage({ props }) {
    const auth = getAuth()
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const notify = () => toast('Login Successful')
    const errorNotification = () => toast.error('Please check your credentials')
    let location = useLocation();
    let history = useHistory()

    const appendBackGroundProperty = () => {
        var element = document.querySelector("body")
        element.classList.add("bg-gradient-primary")
    }

    useEffect(() => {
        appendBackGroundProperty();
    }, [0])

    return (
        <>
            <ToastContainer />
            <Row className="justify-content-center">
                <Col xl="10" lg="12" md="9">
                    <Card className="o-hidden border-0 shadow-lg my-5">
                        <Card.Body className="p-0">
                            <Row>
                                <Col lg="6" className="d-none d-lg-block bg-login-image"></Col>
                                <Col lg>
                                    <div className="p-5">
                                        <div className="text-center">
                                            <h1 className="hr text-gray-900 mb-4">Login</h1>
                                        </div>
                                        <Form className="user">
                                            <Form.Group className="form-group">
                                                <Form.Control
                                                    onChange={e => {
                                                        e.preventDefault();
                                                        setUsername(e.target.value)
                                                    }}
                                                    className="form-control form-control-user" placeholder="Username"
                                                    id="exampleInputEmail" aria-describedby="emailHelp"
                                                ></Form.Control>
                                            </Form.Group>
                                            <Form.Group className="form-group">
                                                <Form.Control
                                                    onChange={e => {
                                                        e.preventDefault();
                                                        setPassword(e.target.value)
                                                    }
                                                    }
                                                    className="form-control form-control-user" placeholder="Password" type="password"
                                                    id="exampleInputPassword"
                                                ></Form.Control>
                                            </Form.Group>
                                            <Form.Group className="form-group">
                                                <div className="custom-control custom-control-checkbox small">
                                                    <Form.Check name="" id="customCheck" className="custom-control-input" label="Remember me"></Form.Check>
                                                </div>
                                            </Form.Group>
                                            <Button
                                                type="submit"
                                                onClick={e => {
                                                    e.preventDefault();
                                                    axios.post("http://127.0.0.1:8000/users/authenticate/", {
                                                        username: username,
                                                        password: password
                                                    })
                                                        .then(response => {
                                                            signInWithEmailAndPassword(auth, response.data.email, password)
                                                                .then((userCredential) => {
                                                                    const user = userCredential.user
                                                                    console.log(user)
                                                                })
                                                                .catch((error) => {
                                                                    console.error(error.message)
                                                                })
                                                            notify()
                                                            localStorage.setItem("token", response.data.token)
                                                            localStorage.setItem("email", response.data.email)
                                                            let { from } = location.state || { from: { pathname: "/" } }
                                                            history.replace(from)
                                                        })
                                                        .catch(error => {
                                                            errorNotification()
                                                            console.error(error)
                                                        })
                                                }
                                                }
                                                className="btn btn-primary btn-user btn-block"
                                            >Login</Button>
                                        </Form>
                                    </div>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </>
    )
}

export default LoginPage;
