import React from 'react';
import {
    Link
} from 'react-router-dom'
import {
    Nav,
    Navbar,
    NavDropdown
} from 'react-bootstrap'
import logo from "../assets/logos/TotalEnergies.webp"
import profile from "../assets/images/Rectangle.webp"
import { useEffect } from 'react';
import axios from 'axios'
import { useState } from 'react';

import { getAuth, signOut } from "firebase/auth";
import Dropdown from '@restart/ui/esm/Dropdown';

function NavBar(props) {
    const auth = getAuth()
    const [user, setUser] = useState({})
    const [jobTitle, setJobTitle] = useState("")
    const [searchTerm, setSearchTerm] = useState("")

    useEffect(() => {
        //get the user details for this page load
        axios.get("http://127.0.0.1:8000/users/get/",
            {
                headers: {
                    "Content-type": "Application/json",
                    "Authorization": `Token ${localStorage.getItem('token')}`
                }
            }
        )
            .then(res => {
                setUser(res.data.user)
                setJobTitle(res.data.job_title)
            })
            .catch(err => console.log(JSON.stringify(err)))
    }, [user.first_name])

    const { handleSearchTerm } = {...props}
    return (
        <>
            {/* Top Bar */}
            <Navbar className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                <Navbar.Brand>
                    <img src={logo} alt="" height="60" />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse>
                    <Nav>
                        <button id="sidebarToggleTop" className="btn btn-link d-md-non rounded-circle mr-3">
                            <i className="fa fa-bars"></i>
                        </button>
                        <form action="" className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                            <div className="input-group">
                                <input type="text" name="" id="" placeholder="Search for..." 
                                    onChange={e => {
                                        e.preventDefault()
                                        handleSearchTerm(e.target.value)
                                    }}
                                    className="form-control bg-light border-0 small" />
                                <div className="input-group-append">
                                    <button className="btn btn-primary">
                                        <i className="fas fa-search fa-sm"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </Nav>
                    <Nav className="navbar-nav nav-item dropdown no-arrow ml-auto">
                        <NavDropdown role="button" id="basic-nav-dropdown" className="mr-2 d-lg-inline text-gray-600 nav-link" id="userDropdown" title={`${user.first_name} ${user.last_name}`} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <NavDropdown.Item href="#" className="dropdown-item">
                                Profile
                            </NavDropdown.Item>
                            <NavDropdown.Item
                                onClick={() => {
                                    localStorage.setItem("token", "")
                                    localStorage.setItem("email", "")
                                    signOut(auth).then(() => {
                                        window.location.reload(false)
                                    })
                                }
                                }
                                className="dropdown-item" href="/">
                                Logout
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}

export default NavBar;