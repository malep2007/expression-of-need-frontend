import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import BasePage from './pages/BasePage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/Register';
import ProtectedRoute from './components/ProtectedRoutes';
import '../src/assets/css/sb-admin-2.css'


function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login">
          <LoginPage></LoginPage>
        </Route>
        <Route path="/register">
          <RegisterPage></RegisterPage>
        </Route>
        <ProtectedRoute path="/">
          <BasePage></BasePage>
        </ProtectedRoute>
      </Switch>
    </Router>
  );
}

export default App;
