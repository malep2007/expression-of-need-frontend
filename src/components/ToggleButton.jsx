import React from 'react'
import { useState } from 'react/cjs/react.development'
import './styles/ToggleButton.css'

function ToggleButton() {
    const [pressed, setPressed] = useState()
    return (
        <>
            <div class="relative inline-block w-10 mr-2 align-middle select-none transition duration-200 ease-in">
                <input
                    onChange={e => {
                        setPressed(true)
                    }} 
                    type="checkbox" name="toggle" id="toggle" class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer" />
                <label for="toggle" class="toggle-label block overflow-hidden h-6 rounded-full bg-gray-300 cursor-pointer"></label>
            </div>
            <label for="toggle" class="text-md text-gray-700">Switch to Table</label>
        </>
    )
}

export default ToggleButton