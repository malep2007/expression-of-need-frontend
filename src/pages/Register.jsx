import axios from 'axios';
import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import {getAuth, createUserWithEmailAndPassword } from 'firebase/auth'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


import logo from "../assets/logos/TotalEnergies.webp"

function RegisterPage() {
    const auth = getAuth();
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [title, setTitle] = useState("")

    return (
        <>
            <ToastContainer />
            <div className="py-20">
                <form className="p-4 mx-auto flex flex-col self-center max-w-md rounded-md border shadow-sm space-y-2">
                    <img alt="" className="w-15 h-15 self-center" src={logo}></img>
                    <p className="font-sans self-center text-xl uppercase font-semibold">Register</p>
                    <div className="border flex flex-row ring-transparent rounded-md mx-8 h-12">
                        <input
                            onChange={e => {
                                e.preventDefault()
                                setFirstName(e.target.value)
                            }}
                            required={true}
                            className="w-full font-sans p-2 text-base" placeholder="First name" type="text"></input>
                    </div>
                    <div className="border flex flex-row ring-transparent rounded-md mx-8 h-12">
                        <input
                            required
                            onChange={e => {
                                e.preventDefault()
                                setLastName(e.target.value)
                            }}
                            className="w-full font-sans p-2 text-base" placeholder="Last name" type="text"></input>
                    </div>
                    <div className="border flex flex-row ring-transparent rounded-md mx-8 h-12">
                        <input
                            onChange={e => {
                                e.preventDefault()
                                setEmail(e.target.value)
                            }}
                            required
                            className="w-full font-sans p-2 text-base" placeholder="Email Address" type="email"></input>
                    </div>
                    <div className="border flex flex-row ring-transparent rounded-md mx-8 h-12">
                        <input
                            onChange={e => {
                                e.preventDefault()
                                setTitle(e.target.value)
                            }}
                            required
                            className="w-full font-sans p-2 text-base" placeholder="Job Title" type="text"></input>
                    </div>
                    <div className="flex flex-row mx-8 justify-between">
                        <button
                            onClick={e => {
                                e.preventDefault()
                                axios.post("http://127.0.0.1:8000/users/add/", {
                                    user: {
                                        first_name: firstName,
                                        last_name: lastName,
                                        email: email
                                    },
                                    job_title: title,
                                    department: 'Test Department'
                                })
                                .then(response => {
                                    createUserWithEmailAndPassword(auth, email, "123test")
                                    .then((userCredential) => {
                                        const user = userCredential.user
                                        console.log(user)
                                    })
                                    .catch((error) => {
                                        console.error(error.message)
                                    })
                                    const notify = () => toast(`Registration Successful`)
                                    notify()
                                })
                                .catch(error => {
                                    const errorNotification = () => toast.error(`${error}`)
                                    errorNotification()
                                })
                            }}
                            className="shadow-md uppercase font-sans font-semibold text-white p-2 bg-blue-700 hover:bg-blue-900 rounded-md w-24">register</button>
                        <button
                            onClick={e => {
                                e.preventDefault()
                                setFirstName("")
                                setLastName("")
                                setEmail("")
                                setTitle("")
                            }}
                            className="shadow-md uppercase font-sans font-semibold text-white p-2 bg-red-700 hover:bg-red-9a00 rounded-md w-24">cancel</button>
                    </div>
                    <p className="font-sans text-sm text-gray-500 self-center mx-30">Already have an account, <Link className="underline font-semibold" to="/login">sign in</Link></p>
                </form>
            </div>
        </>
    )
}

export default RegisterPage;
