import React, { useEffect, useState } from 'react';
import axios from 'axios'


function ExpressionTable(props) {
    const [procStaff, setProcStaff] = useState([])

    const getProcStaff = () => {
        axios.get("http://127.0.0.1:8000/users/procurement/get/",
            {
                headers: {
                    "Content-type": "Application/json",
                    "Authorization": `Token ${localStorage.getItem('token')}`
                }
            }
        )
            .then(response => {
                setProcStaff(response.data)
            })
            .catch(error => console.error(error))
    }

    useEffect(() => {
        getProcStaff()
    }, [procStaff.length])

    const { expressions, searchTerm } = { ...props }
    return (
        <>
            <div className="card shadow mb-4">
                <div className="card-header py-3">
                    <h6 className="m-0 font-weight-bold text-primary">Purchase Requst List</h6>
                </div>
                <div className="card-body">
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>What</th>
                                    <th>Description</th>
                                    <th>Issuer</th>
                                    <th>Department</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Cost</th>
                                    <th>Budget</th>
                                    <th>Location</th>
                                    <th>Delivery Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>What</th>
                                    <th>Description</th>
                                    <th>Issuer</th>
                                    <th>Department</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Cost</th>
                                    <th>Budget</th>
                                    <th>Location</th>
                                    <th>Delivery Date</th>
                                    <th>Status</th>
                                </tr>
                            </tfoot>

                            <tbody>
                                {
                                    expressions.filter(obj => obj.what.includes(searchTerm))
                                        .map(item => (
                                            <tr
                                                onClick={e => {
                                                    e.preventDefault()
                                                    console.log("clicked: navigate to detail")
                                                }}
                                                key={item.id}
                                            >
                                                <td className="px-4 capitalize hover:bg-blue-500"><a href={item.file_url} className="no-underline hover:underline hover:font-bold">{item.what}</a></td>
                                                <td>{item.reason}</td>
                                                <td className="capitalize">{item.issuer.user.first_name} {item.issuer.user.last_name}</td>
                                                <td className="capitalize">{item.issuer.department}</td>
                                                <td>{item.quantity}</td>
                                                <td className="capitalize">{item.unit_of_measurement}</td>
                                                <td>{item.budget_estimate}</td>
                                                <td>{item.budget_type}</td>
                                                <td className="capitalize">{item.location}</td>
                                                <td>{new Date(item.delivery_date).toDateString()}</td>
                                                <td>{item.status}</td>
                                            </tr>
                                        ))
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </>

    )
}

export default ExpressionTable;