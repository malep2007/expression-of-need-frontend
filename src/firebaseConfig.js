import { initializeApp } from "firebase/app";
import { getDownloadURL, getStorage, ref, uploadBytesResumable } from "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyC8BaKIoByFtwxZ34SVy11TpUmB4AoRX28",
    authDomain: "fluttertest-be066.firebaseapp.com",
    databaseURL: "https://fluttertest-be066.firebaseio.com",
    projectId: "fluttertest-be066",
    storageBucket: "fluttertest-be066.appspot.com",
    messagingSenderId: "944486031583",
    appId: "1:944486031583:web:4c1b6f3607a4fc1d17496a"
  };
const firebaseApp = initializeApp(firebaseConfig);
const storage = getStorage(firebaseApp)

function uploadFile(file){
  const fileRef = ref(storage, 'expression/' + file.name)
  const uploadTask = uploadBytesResumable(fileRef, file)
  return uploadTask
}

function getFileUrl(file){
  const fileRef = ref(storage, 'expression/' + file.name)
  return getDownloadURL(fileRef)
}

export {firebaseApp, uploadFile, getFileUrl }



