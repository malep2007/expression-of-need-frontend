import axios from 'axios';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react/cjs/react.development';
import ExpressionTable from '../components/ExpressionTable';
import NavBar from '../components/Navbar';
import SideBar from '../components/SideBar';

function IndexPage(props) {
    const [data, setData] = useState([])
    useEffect(() => {
        axios.get("http://127.0.0.1:8000/expression/",
            {
                headers: {
                    "Content-type": "Application/json",
                    "Authorization": `Token ${localStorage.getItem('token')}`
                }
            })
            .then(res => {
                setData(res.data)
            })
            .catch(error => console.error(error))
    }, [data.length])

    const { searchTerm, heading} = {...props}

    return (
        <>
            {/* Page Heading */}
            <h1 className="h3 mb-4 text-gray-800">{heading}</h1>
            <ExpressionTable expressions={data} searchTerm={searchTerm} />
        </>
    )
}

export default IndexPage;