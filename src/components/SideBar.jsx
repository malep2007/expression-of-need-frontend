import React from 'react'
import { Nav, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function SideBar() {
    return (
        <>
            <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
                <Link to="/" className="sidebar-brand d-flex align-items-center justify-content-center">
                    <div className="sidebar-brand-icon rotate-n-15">
                        <i className="fas fa-laugh-wink"></i>
                    </div>
                    <div className="sidebar-brand-text mx-3">Express<sup>RQ</sup></div>
                </Link>

                <hr className="sidebar-divider my-0" />

                <li className="nav-item">
                    <a href="/" className="nav-link">
                        <i className="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <hr className="sidebar-divider" />
                <Nav>
                    <NavDropdown className="nav-link no-arrow br-white py-2 collapse-inner rounded" title="My Requests">
                        <NavDropdown.Item className="collapse-item">
                            <Link to="/">View Requests</Link>
                        </NavDropdown.Item>
                        <NavDropdown.Item className="collapse-item">
                            <Link className="" to="/new-eon">Create Request</Link>
                        </NavDropdown.Item >
                    </NavDropdown>
                </Nav>
            </ul>
        </>
    )
}

export default SideBar;