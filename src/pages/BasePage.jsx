import React, { useEffect, useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom'
import axios from 'axios';

import IndexPage from '../pages/IndexPage';
import EonForm from '../pages/EonForm';
import SideBar from "../components/SideBar"
import NavBar from "../components/Navbar"

function BasePage() {
    const [data, setData] = useState([])
    const [searchTerm, setSearchTerm] = useState('')

    useEffect(() => {
        axios.get("http://127.0.0.1:8000/expression/",
            {
                headers: {
                    "Content-type": "Application/json",
                    "Authorization": `Token ${localStorage.getItem('token')}`
                }
            })
            .then(res => {
                setData(res.data)
            })
            .catch(error => console.error(error))
    }, [data.length])

    const handleSearchTerm = (searchTerm) => {
        //TODO: set state for search term from child component
        setSearchTerm(searchTerm)
    }

    return (
        <Router>
            {/* page content */}
            <div id="wrapper">
                <SideBar />
                <div id="content-wrapper" className="d-flex flex-column">
                    <div id="content">
                        {/** Navbar component */}
                        <NavBar handleSearchTerm={handleSearchTerm} />

                        {/* Content starts from here */}
                        {/** Begin page Content */}
                        <div className="container-fluid">
                            <Switch>
                                <Route path="/new-eon">
                                    <EonForm heading="Raise Purchase Request"></EonForm>
                                </Route>
                                <Route path="/">
                                    <IndexPage searchTerm={searchTerm} heading="Purchase Requests"/>
                                </Route>
                            </Switch>
                        </div>
                    </div>

                    {/* Begin Footer */}
                    <footer class="sticky-footer bg-white">
                        <div className="container my-auto">
                            <div className="copyright text-center my-auto">
                                <span>Copyright &copy; TotalEnergies Uganda Limited</span>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </Router>
    )
}

export default BasePage;