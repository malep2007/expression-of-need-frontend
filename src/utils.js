const axios = require('axios').default;

async function authenticateUser({username, password}){
    try{
        const response = await axios.post("http://127.0.0.1:8000/users/authenticate/", {
            username,
            password
        })
        return response
    }
    catch(error){
        return error
    }
}

export default authenticateUser;