import React from 'react';

import ExpressionTable from '../components/ExpressionTable'

export default {
  title: 'Expression/ExpressionTable',
  component: ExpressionTable,
};

const Template = (args) => <ExpressionTable {...args}/>

export const Empty = Template.bind({});
Empty.args = {
    expressions: []
}

export const FullTable = (args) => <ExpressionTable {...args}/>
FullTable.args ={
    expressions: [
        {
            "id": 81,
            "reason": "Medicine Cabinet",
            "location": "Head Office",
            "what": "Medicine Cabinet",
            "validation": "No Validation",
            "status": "New",
            "recommended_suppliers": "CIPLA",
            "issuer": {
                "user": {
                    "first_name": "Jamila",
                    "last_name": "Lubega",
                    "email": "jamila.lubega@totalenergies.com"
                },
                "department": "Finance",
                "job_title": "Procurement Controller"
            },
            "delivery_date": "2021-09-21",
            "budget_estimate": 1200000,
            "quantity": 1,
            "unit_of_measurement": "unit",
            "budget_type": "CAPEX",
            "updated_at": "2021-09-14 17:23:41",
            "created_at": "2021-09-14 17:23:41",
            "file_url": null
        },
        {
            "id": 80,
            "reason": "t-shirts",
            "location": "Head office",
            "what": "T-shirts",
            "validation": "No Validation",
            "status": "New",
            "recommended_suppliers": "Nine Designers",
            "issuer": {
                "user": {
                    "first_name": "Jamila",
                    "last_name": "Lubega",
                    "email": "jamila.lubega@totalenergies.com"
                },
                "department": "Finance",
                "job_title": "Procurement Controller"
            },
            "delivery_date": "2021-09-28",
            "budget_estimate": 120,
            "quantity": 120,
            "unit_of_measurement": "piece",
            "budget_type": "CAPEX",
            "updated_at": "2021-09-14 11:03:22",
            "created_at": "2021-09-14 11:03:22",
            "file_url": null
        },
        {
            "id": 79,
            "reason": "jeans",
            "location": "Head office",
            "what": "Jeans",
            "validation": "No Validation",
            "status": "New",
            "recommended_suppliers": "jean supplier",
            "issuer": {
                "user": {
                    "first_name": "Jamila",
                    "last_name": "Lubega",
                    "email": "jamila.lubega@totalenergies.com"
                },
                "department": "Finance",
                "job_title": "Procurement Controller"
            },
            "delivery_date": "2021-09-14",
            "budget_estimate": 120000,
            "quantity": 12,
            "unit_of_measurement": "unit",
            "budget_type": "CAPEX",
            "updated_at": "2021-09-14 11:00:51",
            "created_at": "2021-09-14 11:00:51",
            "file_url": null
        }
    ]
}