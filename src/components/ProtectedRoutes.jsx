import React from 'react';
import { Redirect } from 'react-router';
import { Route, useHistory, useLocation } from 'react-router-dom'





function ProtectedRoute({ children, ...rest }) {
    let history = useHistory()
    let location = useLocation()
    let {from} = history.location.state = {
        pathname: location.pathname
    }

    const checkToken = () => {
        return (
            localStorage.getItem('token') ? (children) : (
            <Redirect
                to="/login"
                from={from}
            />
            )
        )
    }

    return (
        <Route
            {...rest}
            render={ checkToken }
        >
        </Route>
    )
}

export default ProtectedRoute;